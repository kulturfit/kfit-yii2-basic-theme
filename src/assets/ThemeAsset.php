<?php

namespace kfit\theme\assets;

use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@kfit/theme/base/';
    public $css = [
        'css/font-awesome.min.css',
        'css/elegent.min.css',
        'css/helper.css',
        'css/style.css',
        'css/menu.css',
        'css/vendors.css',
        'css/icon_fonts/css/all_icons_min.css',
        'css/custom.css'
    ];
    public $js = [
        'vendor/ckeditor/ckeditor.js',
        'vendor/ckeditor/lang/es.js',
        'js/feather.min.js',
        'js/plugins.js',
        'js/common_scripts.min.js',
        'js/functions.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
