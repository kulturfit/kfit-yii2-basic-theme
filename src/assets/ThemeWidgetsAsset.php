<?php

namespace kfit\theme\assets;

use yii\web\AssetBundle;

class ThemeWidgetsAsset extends AssetBundle
{
    public $sourcePath = '@kfit/theme/widgets/assets/';
    public $css = [
        'newsletter/main.css',
        'searchbar/main.css',
        'slider/main.css'
    ];
    public $js = [
        'slider/main.js'
    ];
    public $depends = [
        'kfit\theme\assets\ThemeAsset',
    ];
}
