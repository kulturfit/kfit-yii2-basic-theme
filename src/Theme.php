<?php

namespace kfit\theme;

use yii\base\Theme as YiiTheme;
use Yii;

/**
 * Esta Clase muestra contenidos HTML con bootstrap
 * @package ticmakers
 * @subpackage themes
 * @category Theme
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Theme extends YiiTheme
{
    const BS4_ASSET = 'yii\bootstrap4\BootstrapAsset';
    const BSP4_ASSET = 'yii\bootstrap4\BootstrapPluginAsset';

    private $_mainLayout = 'main';
    private $_pathMap = ['@app/views' => '@kfit/theme/views'];
    private $_imagesBaseUrl = '@web/images';

    public function init()
    {
        parent::init();

        $baseUrl = Yii::$app->assetManager->getPublishedUrl('@kfit/theme/base/');
        $this->setBaseUrl($baseUrl);
        $bundles = Yii::$app->getAssetManager()->bundles;

        if (!isset($bundles[static::BS4_ASSET])) {
            Yii::$app->getAssetManager()->bundles[static::BS4_ASSET] = [
                'sourcePath' => '@kfit/theme/base',
                'css' => [
                    'css/bootstrap.min.css',
                ],
            ];
        }

        if (!isset($bundles[static::BSP4_ASSET])) {
            Yii::$app->getAssetManager()->bundles[static::BSP4_ASSET] = [
                'sourcePath' => '@kfit/theme/base',
                'js' => [
                    'js/popper.min.js',
                    'js/bootstrap.min.js',
                ],
            ];
        }
    }

    /**
     * Get the value of _mainLayout
     */
    public function getMainLayout()
    {
        return $this->_mainLayout;
    }

    /**
     * Set the value of _mainLayout
     *
     * @return  self
     */
    public function setMainLayout($mainLayout)
    {
        $this->_mainLayout = $mainLayout;

        return $this;
    }

    /**
     * Get the value of _imagesBaseUrl
     */
    public function getImagesBaseUrl()
    {
        return $this->_imagesBaseUrl;
    }

    /**
     * Set the value of _imagesBaseUrl
     *
     * @return  self
     */
    public function setImagesBaseUrl($imagesBaseUrl)
    {
        $this->_imagesBaseUrl = $imagesBaseUrl === null ? null : rtrim(Yii::getAlias($imagesBaseUrl), '/');

        return $this;
    }

    /**
     * Converts a relative URL into an absolute URL using [[baseUrl]].
     * @param string $url the relative URL to be converted.
     * @return string the absolute URL
     * @throws InvalidConfigException if [[baseUrl]] is not set
     */
    public function getImageUrl($url)
    {
        if (($baseUrl = $this->getImagesBaseUrl()) !== null) {
            return $baseUrl . '/' . ltrim($url, '/');
        }

        throw new InvalidConfigException('The "imagesBaseUrl" property must be set.');
    }
}
