<?php
use app\helpers\Html;
use kfit\theme\widgets\Menu;
use kfit\adm\models\app\Menus;
use yii\helpers\Url;
?>
<!--=============================================
	=            Header         =
    =============================================-->

<header class="header_sticky">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-6">
                <div id="logo_home">
                    <h1 class="logo-home-text"><a href="<?= Url::to(['//site/index']) ?>" title="Findoctor" style="background: url('<?= $this->theme->getImageUrl('logo-horizontal.png') ?>');background-size: contain;background-repeat: no-repeat;background-position: center;">Findoctor</a></h1>
                </div>
            </div>
            <nav class="col-lg-9 col-6">
                <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="#0"><span>Menu mobile</span></a>
                <ul id="top_access">

                    <?php if (Yii::$app->user->isGuest) : ?>
                        <li>
                            <a href="<?= Url::to(['//site/login']) ?>" class="load-modal"><i class="pe-7s-user" title="Iniciar sesión"></i></a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['//site/pre-register']) ?>" class="load-modal"><i class="pe-7s-add-user" title="Registro"></i></a>
                        </li>
                    <?php else : ?>
                        <li>
                            <a class="btn btn-sm btn-my-account" href="<?= Url::to(['//my-account']) ?>">
                                <em class="fa fa-user"></em> <?= Yii::t('app', 'Mi cuenta') ?>
                            </a>
                        </li>
                        <li>
                            <a class="btn btn-logout btn-outline-primary btn-sm" data-method="post" href="<?= Url::to(['//site/logout']) ?>">
                                <em class="fa fa-sign-out-alt "></em> <?= Yii::t('app', 'Salir') ?>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <div class="main-menu">

                    <?php
                    $menu = Menus::find()
                        ->where([
                            'active' => Menus::STATUS_ACTIVE,
                            'position' => 'header',
                        ])
                        ->one();
                    echo Menu::widget([
                        'items' => !empty($menu) ? $menu->getItems() : [],
                    ]);

                    ?>

                </div>
                <!-- /main-menu -->
            </nav>
        </div>
    </div>
    <!-- /container -->
</header>
<!-- /header -->