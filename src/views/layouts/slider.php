	<!--=============================================
	=            Hero slider Area         =
	=============================================-->

	<div class="hero-slider-container mb-35">
	    <!--=======  Slider area  =======-->

	    <div class="hero-slider-one">
	        <!--=======  hero slider item  =======-->

	        <div class="hero-slider-item slider-bg-1">
	            <div class="slider-content d-flex flex-column justify-content-center align-items-center">
	                <h1>Organic Farm</h1>
	                <p>get fresh food from our firm to your table</p>
	                <a href="shop-left-sidebar.html" class="slider-btn">SHOP NOW</a>
	            </div>
	        </div>

	        <!--=======  End of hero slider item  =======-->


	        <!--=======  Hero slider item  =======-->

	        <div class="hero-slider-item slider-bg-2">
	            <div class="slider-content d-flex flex-column justify-content-center align-items-center">
	                <h1>Fresh & Nature</h1>
	                <p>get fresh food from our firm to your table</p>
	                <a href="shop-left-sidebar.html" class="slider-btn">SHOP NOW</a>
	            </div>
	        </div>

	        <!--=======  End of Hero slider item  =======-->

	    </div>

	    <!--=======  End of Slider area  =======-->

	</div>

	<!--=====  End of Hero slider Area  ======--> 