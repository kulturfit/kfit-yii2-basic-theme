<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap4\Html;
use kfit\core\helpers\MessageHelper;
use kfit\core\helpers\StringsHelper;
use app\assets\AppAsset;
use kartik\icons\FontAwesomeAsset;

AppAsset::register($this);
FontAwesomeAsset::register($this);

$this->registerJs("
    window.TEXT_EMPTY = '" . Yii::$app->strings::getTextEmpty() . "';
    bootbox.setLocale('" . Yii::$app->language . "');
    " . Yii::$app->message::getMessagesJS() . "

    let loadModalElements = document.getElementsByClassName('load-modal');
    for (let element of loadModalElements) {
        element.addEventListener('click', function (evt) {
            evt.preventDefault();
        });
    }
", \yii\web\View::POS_LOAD);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>