<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use kfit\theme\widgets\productslider\ProductSlider;
use app\assets\FontAwesomeAsset;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use kfit\core\helpers\StringsHelper;
use kfit\core\helpers\MessageHelper;
use yii\bootstrap4\Modal;
use kfit\core\widgets\Breadcrumbs;
use kfit\adm\models\base\Menus;
use kfit\theme\widgets\AccordianMenu\AccordianMenu;
AppAsset::register($this);
FontAwesomeAsset::register($this);

$this->registerJs("
    window.TEXT_EMPTY = '" . Yii::$app->strings::getTextEmpty() . "';
    bootbox.setLocale('" . Yii::$app->language . "');
    " . Yii::$app->message::getMessagesJS() . "

    let loadModalElements = document.getElementsByClassName('load-modal');
    for (let element of loadModalElements) {
        element.addEventListener('click', function (evt) {
            evt.preventDefault();
        });
    }
", \yii\web\View::POS_LOAD);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>
    <style>
        .header-bottom {
            background-color: #28a745;
        }
    </style>

</head>

<body>
    <?php $this->beginBody() ?>

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>
    <?php $this->beginContent('@theme/views/layouts/header.php'); ?><?php $this->endContent(); ?>
    <main>
        <div id="breadcrumb">
            <div class="container">
                <?= Breadcrumbs::widget([
                    'options' => ['class' => ''],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
                ]) ?>
            </div>
        </div>
        <div class="container mt-30">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <div id="jquery-accordion-menu" class="jquery-accordion-menu white">
                        <?php
                        $menus = Menus::find()
                            ->where([
                                'active' => Menus::STATUS_ACTIVE,
                                'position' => 'dashboard-left',
                            ])
                            ->all();
                        foreach ($menus as $menu) {
                            echo AccordianMenu::widget([
                                'linkTemplate' => '<a data-method="post" href="{url}" {data-toggle} {data-target-submenu}><i class="fa {icon}"></i> {label}</a>',
                                'submenuTemplate' => '<a data-method="post" href="{url}" {data-toggle} {data-target-submenu}><i class="fa {icon}"></i> {label}</a><ul class="submenu">{items}</ul>',
                                'options' => ['class' => ''],
                                'items' => !empty($menu) ? $menu->getItems() : [],
                            ]);
                        }
                        ?>
                    </div>
                </div>
                <div class="col-lg-9 col-12 mb-10">
                    <div class="tab-content">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php $this->beginContent('@theme/views/layouts/footer.php'); ?>
    <?php $this->endContent(); ?>
    <?php if (isset($this->blocks['outcontainer'])) : ?>
        <?= $this->blocks['outcontainer'] ?>
    <?php endif; ?>
    <div id="toTop"></div>
    <?php Modal::begin([
        'id' => 'default-modal',
        'title' => Yii::t('app', '{title}'),
        'size' => Modal::SIZE_DEFAULT,
        'options' => [
            'style' => 'display: none;',
            'tabindex' => false,
        ],
        'clientOptions' => [
            'backdrop' => 'static',
            'keyboard' => false,
        ],
    ]);
    Modal::end();
    ?>
    <div id="move-modal"></div>
    <?php $this->endBody() ?>
</body>
<?php $this->endPage() ?>