<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use kfit\theme\widgets\productslider\ProductSlider;
use app\assets\FontAwesomeAsset;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use kfit\core\helpers\StringsHelper;
use kfit\core\helpers\MessageHelper;
use kfit\core\widgets\Breadcrumbs;
use kfit\adm\models\base\Menus;
use kfit\theme\widgets\AccordianMenu\AccordianMenu;

AppAsset::register($this);
FontAwesomeAsset::register($this);

$this->registerJs("
    window.TEXT_EMPTY = '" . Yii::$app->strings::getTextEmpty() . "';
    bootbox.setLocale('" . Yii::$app->language . "');
    " . Yii::$app->message::getMessagesJS() . "

    let loadModalElements = document.getElementsByClassName('load-modal');
    for (let element of loadModalElements) {
        element.addEventListener('click', function (evt) {
            evt.preventDefault();
        });
    }
", \yii\web\View::POS_LOAD);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
</head>

<body>
    <?php $this->beginBody() ?>
    <div class="layer"></div>
    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>
    <?php $this->beginContent('@theme/views/layouts/header.php'); ?>
    <?php $this->endContent(); ?>
    <main>
        <div id="breadcrumb">
            <div class="container">
                <?= Breadcrumbs::widget([
                    'options' => ['class' => ''],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
                ]) ?>
            </div>
        </div>
        <div class="container mt-30 mb-30">
            <div class="row">

                <div class="col-lg-12 col-12 mb-4">
                    <div class="tab-content">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php $this->beginContent('@theme/views/layouts/footer.php'); ?>
    <?php $this->endContent(); ?>
    <?php $this->endBody() ?>
    <div id="move-modal"></div>
    <?php Modal::begin([
        'id' => 'default-modal',
        'title' => Yii::t('app', '{title}'),
        'size' => Modal::SIZE_DEFAULT,
        'options' => [
            'style' => 'display: none;',
            'tabindex' => false,
        ],
        'clientOptions' => [
            'backdrop' => 'static',
            'keyboard' => false,
        ],
    ]);
    Modal::end();
    ?>
</body>
<?php $this->registerCss('.logo-home-text, .newsletter-title-text{ display: block !important; }') ?>
<?php $this->endPage() ?>