<?php

use kfit\theme\widgets\footernavigation\FooterNavigation;
use kfit\theme\widgets\newsletter\NewsLetter;

$data = Yii::$app->ui::footerNavigationData() ?? [];
$original = Yii::$app->ui::footerNavigationData() ?? [];
unset($original['items']);
$aditionals = $original;
?>
<footer>
	<?= NewsLetter::widget() ?>
	<div class="container margin_60_35">
		<?= FooterNavigation::widget([
			'data' => $data['items'] ?? [],
			'aditionals' => $aditionals
		]) ?>
	</div>
</footer>
<script src="<?= Yii::$app->getModule('adm')->socialAuthConfig['firebaseUrl'] ?>"></script>