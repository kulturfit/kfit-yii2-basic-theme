<!--=======  newsletter section  =======-->

<div class="newsletter-section pt-30 pb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 mb-sm-20 mb-xs-20">
                <!--=======  newsletter title =======-->

                <div class="newsletter-title">
                    <h1 class="newsletter-title-text">
                        <img src="<?= $this->theme->getUrl('images/icon-newsletter.png') ?>" alt="">
                        <?= $this->context->title ?>
                    </h1>
                </div>

                <!--=======  End of newsletter title  =======-->
            </div>

            <div class="col-lg-8 col-md-12 col-sm-12">
                <!--=======  subscription-form wrapper  =======-->

                <div class="subscription-form-wrapper d-flex flex-wrap flex-sm-nowrap">
                    <p class="mb-xs-20"><?= Yii::t('app', $this->context->subTitle, ['sistema' => Yii::$app->name]) ?></p>
                    <div class="subscription-form">
                        <form id="mc-form" class="mc-form subscribe-form">
                            <input type="email" id="mc-email" autocomplete="off" placeholder="<?= $this->context->placeholder ?>">
                            <button id="mc-submit" type="submit"> <?= $this->context->textButton ?></button>
                        </form>

                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts">
                            <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                            <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                            <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div><!-- mailchimp-alerts end -->
                    </div>
                </div>

                <!--=======  End of subscription-form wrapper  =======-->
            </div>
        </div>
    </div>
</div>

<!--=======  End of newsletter section  =======-->