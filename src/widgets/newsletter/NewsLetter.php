<?php
namespace kfit\theme\widgets\newsletter;

use yii\base\Widget;

class NewsLetter extends Widget
{
    public $title = 'Boletín informativo';
    public $subTitle = 'Suscríbase y reciba las últimas noticias de {sistema}';
    public $placeholder = 'Ingrese su correo electrónico';
    public $textButton = 'Suscribirse';
    /**
     * Undocumented function
     *
     * @return void
     */
    public function run()
    {
        return $this->render('main');
    }

}
