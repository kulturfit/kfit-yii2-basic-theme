<?php

namespace kfit\theme\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use app\helpers\Html;
use kfit\core\widgets\Menu as BaseMenu;
use yii\widgets\Menu as YiiMenu;
use yii\helpers\Url;

class Menu extends BaseMenu
{

    /**
     * Renders the menu.
     */
    public function run()
    {

        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');

            echo Html::tag($tag, $this->renderItems($items), $options);
        }
    }
    /**
     * Creates a widget instance and runs it.
     * The widget rendering result is returned by this method.
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @return string the rendering result of the widget.
     * @throws \Exception
     */
    public static function widget($config = [])
    {
        if (!isset($config['options']['class'])) {
            $config['options']['class'] = '';
        }
        if (!isset($config['options']['style'])) {
            $config['options']['style'] = 'justify-content: center;display: flex;';
        }

        if (!isset($config['itemOptions']['class'])) {
            $config['itemOptions']['class'] = '';
        }

        if (!isset($config['encodeLabels'])) {
            $config['encodeLabels'] = false;
        }
        if (!isset($config['activeCssClass'])) {
            $config['activeCssClass'] = 'active';
        }
        if (!isset($config['linkTemplate'])) {
            $config['linkTemplate'] = '<a data-method="post" class="{activo}" href="{url}" {data-toggle} {data-target-submenu}> {label} </a>';
        }
        if (!isset($config['submenuTemplate'])) {
            $config['submenuTemplate'] = '<a class=" " {data-toggle} {data-target-submenu} aria-expanded="false">
     {label}
     </a>
     <ul class="sub-menu">{items}</ul>';
        }
        if (!isset($config['activateParents'])) {
            $config['activateParents'] = true;
        }

        return parent::widget($config);
    }

    /**
     * Recursively renders the menu items (without the container tag).
     * @param array $items the menu items to be rendered recursively
     * @return string the rendering result
     */
    protected function renderItems($items, $sub = false)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {

            $options = array_merge($this->itemOptions,
                ArrayHelper::getValue($item, 'options', []));

            if (!empty($item['items'])) {
                $item['label'] = $item['label'];
                $options['class']= 'menu-item-has-children ';
            }

            if (!isset($item['url'])) {
                $options['class'] = 'site-menu-category open';
            }

            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];

            if ($item['active']) {
                $class[] = 'open';
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            if (!empty($item['items'])) {
                $idSubMenu = Yii::$app->security->generateRandomString();
                $menu = $this->renderItem($item, $idSubMenu, $sub);
            } else {
                $menu = $this->renderItem($item, '', $sub);
            }

            if (!empty($item['items'])) {

                $submenuTemplate = ArrayHelper::getValue($item,
                    'submenuTemplate',
                    $this->submenuTemplate);
                $menu .= strtr($submenuTemplate,
                    [
                        '{id-submenu}' => $idSubMenu,
                        '{items}' => $this->renderItems($item['items'], true),
                        '{data-target-submenu}' => "href=\"#{$idSubMenu}\"",
                        '{data-toggle}' => !empty($idSubMenu) ? 'data-toggle="collapse"' : '',
                        '{url}' => Html::encode(Url::to($item['url'])),
                        '{label}' => $item['label'],
                        '{activo}' => $class,
                        '{icon}' => isset($item['icon']) ? $item['icon'] : (isset($item['items']) ? '' : ''),
                        '{arrow}' => isset($item['items']) ? '<em class="fa fa-caret-down "></em>' : '',
                    ]);
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }

}
