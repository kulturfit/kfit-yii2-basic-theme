<?php

namespace kfit\theme\widgets\StepBox;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Widget para 
 *
 * @package app
 * @subpackage themes\widgets\StepBox
 * @category Widget
 *
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class StepBox extends \yii\base\Widget
{
    const NUM_COLUMN_CONTAINER = 12;
    const BOX_CLASS = 'box_feat';
    const ROW_CLASS = 'row';

    /**
     * Opciones html para el contenedor principal
     * 
     * @var array
     */
    public $containerOpts = [];

    /**
     * Opciones html para las cajas
     * 
     * @var array 
     */
    public $boxOpts = [];

    /**
     * Items que se mostrarán, cada uno con la siguiente estructura:
     * [
     *     'title' => '',
     *     'message' => '',
     *     'image' => '' 
     * ]
     */
    public $items = [];


    /**
     * Método inicializador del widget
     */
    public function init()
    {
        parent::init();
        $this->containerOpts = ArrayHelper::merge($this->containerOpts, [
            'class' => [static::ROW_CLASS]
        ]);
        $this->boxOpts = ArrayHelper::merge($this->boxOpts, [
            'class' => [static::BOX_CLASS]
        ]);
    }

    /**
     * Ejecución del widget
     * 
     * @return mixed
     */
    public function run()
    {
        $boxes = $this->renderBoxes();
        return Html::tag('div', $boxes, $this->containerOpts);
    }

    /**
     * Crear la estructura HTML de las cajas
     * 
     * @return string
     */
    public function renderBoxes()
    {
        $columnClass = 'col-lg-'. (static::NUM_COLUMN_CONTAINER / count($this->items));
        $boxOpts = $this->boxOpts;
        $boxes = array_map(function($data) use ($columnClass, $boxOpts) {
            $content = Html::tag('span', '') . Html::tag('h3', Html::encode($data['title'])) . Html::tag('p', $data['message']);
            if (isset($data['options'])) {
                $boxOpts = ArrayHelper::merge($boxOpts, $data['options']);
            }
            return Html::tag('div', Html::tag('div', $content, ArrayHelper::merge($boxOpts, [
                'style' => "background:#fff url('". $data['image'] ."') no-repeat center 45px;"
            ])), ['class' => [$columnClass, 'd-flex']]);
        }, $this->items);
        $boxes[count($this->items)-1] = str_replace('<span></span>', '', $boxes[count($this->items)-1]);
        return implode('', $boxes);
    }
}