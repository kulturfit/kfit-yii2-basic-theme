<div class="tab-slider-wrapper">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist"><?= $links ?></div>
    </nav>
    <div class="tab-content" id="nav-tabContent"><?= $tabs ?></div>
</div>