<?php

namespace kfit\theme\widgets\Tabs;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

/**
 * Extención de funcionalidad de tabs.
 *
 * @package app
 * @subpackage themes\widgets\Tabs
 * @category Widgets
 *
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class Tabs extends \yii\base\Widget
{
    /**
     * Items que se renderizarán en el tab, cada uno de los items posee la siguiente estructura:
     * ```
     * [
     *     'label' => 'default Label',
     *     'content' => 'content',
     *     'active' => true|false 
     * ]
     * ```
     * 
     * @var array
     */
    public $items;

    public function init()
    {
        parent::init();
        if (is_null($this->items) || !is_array($this->items)) {
            throw new InvalidConfigException(Yii::t('app', 'You should define a value for the "items" property.'));
        }
    }

    public function run()
    {
        $links = array_map(function ($value) {
            $name = strtolower(str_replace(' ', '', $value['label']));
            $activeClass = (isset($value['active']) && $value['active']) ? ' active' : '';
            $id = $name . '-tab';
            $url = '#' . $name;
            return Html::a($value['label'], $url, [
                'class' => 'nav-item nav-link' . $activeClass,
                'id' => $id,
                'data-toggle' => 'tab',
                'role' => 'tab',
                'aria-selected' => 'true'
            ]);
        }, $this->items);
        $tabs = array_map(function ($value) {
            $content = (!isset($value['content'])) ? '' : $value['content'];
            $activeClass = (isset($value['active']) && $value['active']) ? ' show active' : '';
            $name = strtolower(str_replace(' ', '', $value['label']));
            $id = $name . '-tab';
            return Html::tag('div', $content, [
                'class' => 'tab-pane fade'. $activeClass,
                'id' => $name,
                'role' => 'tabpanel',
                'aria-labelledby' => $id
            ]);
        }, $this->items);
        return $this->render('main.php', [
            'links' => implode($links),
            'tabs' => implode($tabs)
        ]);
    }
}