<?php

use yii\helpers\Url;
use kfit\adm\models\base\Menus;

?>
<div class="footer-navigation-section pb-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <p class="logo-footer">
                    <a href="<?= Url::to(['/site/index'], true) ?>" title="<?= Yii::$app->name ?>">
                        <img class="logo-footer img-fluid" src="<?= Url::to('@web/images/logo.png') ?>" alt="">
                    </a>
                </p>
            </div>
            <?php foreach ($data as $menu) : ?>
                <div class="<?= $itemContentOptions['class'] ?? '' ?>">
                    <h5><?= $menu['title'] ?></h5>
                    <ul class="links">
                        <?php foreach ($menu['items'] as $item) : ?>
                            <li>
                                <a href="<?= ($item['internal']) == Menus::STATUS_ACTIVE ? Url::to($item['url']) : $item['link'] ?>" target="<?= $item['target'] ?? '_self' ?>"><?= $item['label'] ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endforeach ?>
            <div class="col-lg-3 col-md-4">
                <h5><?= Yii::t('app', 'Contact with us') ?></h5>
                <ul class="contacts">
                    <li><a href="tel://<?= Yii::$app->getModule('parameters')->getParameterByCode('TELEFONO') ?>"><span class="icon_mobile"></span> <?= Yii::$app->getModule('parameters')->getParameterByCode('TELEFONO') ?></a></li>
                    <li><a href="mailto:<?= Yii::$app->getModule('parameters')->getParameterByCode('CORREO') ?>"><span class="icon_mail_alt"></span> <?= Yii::$app->getModule('parameters')->getParameterByCode('CORREO') ?></a></li>
                </ul>
                <div class="follow_us">
                    <h5><?= Yii::t('app', 'Follow us') ?></h5>
                    <ul>
                        <?php if (Yii::$app->getModule('parameters')->getParameterByCode('FACEBOOK')) : ?>
                            <li>
                                <a href="<?= Yii::$app->getModule('parameters')->getParameterByCode('FACEBOOK') ?? '#' ?>"><i class="kfit kfit-facebook"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->getModule('parameters')->getParameterByCode('TWITTER')) : ?>
                            <li>
                                <a href="<?= Yii::$app->getModule('parameters')->getParameterByCode('TWITTER') ?? '#' ?>"><i class="social_twitter"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->getModule('parameters')->getParameterByCode('LINKEDIN')) : ?>
                            <li>
                                <a href="<?= Yii::$app->getModule('parameters')->getParameterByCode('LINKEDIN') ?? '#' ?>"><i class="social_linkedin"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->getModule('parameters')->getParameterByCode('INSTAGRAM')) : ?>
                            <li>
                                <a href="<?= Yii::$app->getModule('parameters')->getParameterByCode('INSTAGRAM') ?? '#' ?>"><i class="kfit kfit-instagram"></i></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($aditionals['urlTerms']) && isset($aditionals['copyholder'])) : ?>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <ul id="additional_links">
                <li><a href="<?= $aditionals['urlTerms'] ?>"><?= Yii::t('app', 'Terms and conditions') ?></a></li>
            </ul>
        </div>
        <div class="col-md-4">
            <div id="copy">© <?= date('Y') . ' ' . $aditionals['copyholder'] ?></div>
        </div>
    </div>
<?php endif; ?>