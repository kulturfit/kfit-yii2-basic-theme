<?php
namespace kfit\theme\widgets\footernavigation;

use yii\base\Widget;

class FooterNavigation extends Widget
{
    public $data = [];
    public $itemContentOptions = ['class' => 'col-lg-3 col-md-3 col-sm-6 col-xs-12 mb-xs-30'];
    public $aditionals = [];

    /**
     * Undocumented function
     *
     * @return void
     */
    public function run()
    {
        return $this->render('main', [
            'data' => $this->data,
            'itemContentOptions' => $this->itemContentOptions,
            'aditionals' => $this->aditionals
        ]);
    }

}
