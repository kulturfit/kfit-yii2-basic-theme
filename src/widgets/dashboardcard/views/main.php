<?php
use yii\helpers\Url;
use app\helpers\Html;

?>
<!--=============================================
	=            Dashboard Card          =
	=============================================-->
<style>
    .dashboard .card-body {
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        align-items: center;
    }

    .dashboard .card {
        min-height: 8rem;
    }

    .dashboard i,
    .dashboard svg {
        font-size: 2.8rem;
    }

    .dashboard .card-body:hover {
        cursor: pointer;
        color: var(--button-primary-hover);
    }

    .dashboard p.title {
        font-size: 1.6rem;
        font-weight: bold;
        margin-bottom: 0.2rem;
    }

    .dashboard p:hover {
        cursor: pointer;
    }

    .dashboard .card:hover {
        cursor: pointer;
        color: var(--button-primary-hover);
    }
</style>
<div class="myaccount-content dashboard mb-20">
    <h3><?= $this->context->title ?></h3>
    <div class="row">
        <?php foreach (($this->context->data ?? []) as $item) : ?>
            <div class="col-<?= $item['width'] ?> mb-30">
                <div class="card">
                    <div class="card-body row">
                        <div class="col-2">
                            <i class="<?= $item['icon'] ?? 'fa fa-users' ?>"></i>
                        </div>
                        <div class="col-10">
                            <p class="title text-center"><?= $item['title'] ?? '' ?></p>
                            <p class="text-center"><?= $item['subtitle'] ?? '' ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>

</div>

<!--=====  End of Dashboard Card   ======-->