<?php
namespace kfit\theme\widgets\dashboardcard;

use yii\base\Widget;

class DashboardCard extends Widget
{
    /**
     * Título de la sección
     * 
     * @var string
     */
    public $title;

    /**
     * Arreglo con los items que se mostrarán
     * 
     * @var array
     */
    public $data = [];


    /**
     * Método para la ejecución del widget
     *
     * @return void
     */
    public function run()
    {
        $this->data = array_map(function ($data) {
            return array_merge([
                'width' => 4
            ], $data);
        }, $this->data);
        return $this->render('main');
    }
}
