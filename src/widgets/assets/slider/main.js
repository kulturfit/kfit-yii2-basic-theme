(function ($) {

        "use strict";

        $(window).on("load", function () {

            setTimeout(() => {

                // Carousel
                let owl = $('#carousel-home').owlCarousel({
                    center: true,
                    items: 2,
                    loop: true,
                    margin: 10,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 4
                        }
                    },
                    onInitialize: callback
                });
                
            }, 500);
        
        
        
        });

function callback(event) {
    // $('.rating-carousel').rating('refresh',{
    //     'displayOnly':true,
    //     'showCaption':false,
    //     'size':'xs',
    //     'theme': 'krajee-fa'
    // });
}

})(window.jQuery);