<?php
use yii\helpers\Url;
?>
<!--=======  copyright section  =======-->
<div class="row">
    <div class="col-md-8">
        <ul id="additional_links">
            <?php

            if ($this->context->linkTermsAndConditions ?? false) : ?>
                <li><a href="<?= Url::to($this->context->linkTermsAndConditions ?? '#0') ?>" target="_blank"><?= Yii::t('app','Terms and conditions')?></a></li>
            <?php endif; ?>
            <?php if ($this->context->linkPrivacy ?? false) : ?>
                <li><a href="<?= Url::to($this->context->linkPrivacy ?? '#0') ?>" target="_blank">Privacy</a></li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="col-md-4">
        <div id="copy">© <?= $this->context->year ?> <?= $this->context->company ?></div>
    </div>
</div>

<!--=======  End of copyright section  =======-->