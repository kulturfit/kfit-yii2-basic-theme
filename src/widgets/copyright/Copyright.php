<?php
namespace kfit\theme\widgets\copyright;

use yii\base\Widget;

class Copyright extends Widget
{
    public $year;
    public $company;
    public $linkCompany;
    public $linkTermsAndConditions;
    public $linkPrivacy;
    /**
     * Undocumented function
     *
     * @return void
     */
    public function run()
    {
        if (empty($this->year)) {
            $this->year = date('Y');
        }
        return $this->render('main');
    }
}
