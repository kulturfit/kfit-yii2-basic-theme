<?php
use yii\web\View;

?>
<div class="product-image-slider d-flex flex-custom-xs-wrap flex-sm-nowrap align-items-center mb-sm-35">
    <div class="product-small-image-list">
        <div class="nav small-image-slider-single-product" role="tablist">
            <?php foreach ($images as $key => $value): ?>
                <div class="single-small-image img-full<?= ((isset($value['principal']) && $value['principal'])) ? ' slick-slide' : '' ?>">
                    <a data-toggle="tab" id="single-slide-tab-<?= $key ?>" href="#single-slide<?= $key ?>">
                        <img src="<?= $value['url'] ?>" class="img-fluid" alt="">
                    </a>
                </div>
            <?php endforeach; ?>
            <?php if (!empty($video)): ?>
                <div class="single-small-image img-full">
                    <a data-toggle="tab" id="single-slide-tab-video" href="#single-slide-video">
                        <img src="<?= Yii::getAlias('@web/images/video.png') ?>" alt="">
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="tab-content product-large-image-list">
        <?php foreach ($images as $key => $value): ?>
            <div class="tab-pane fade<?= (isset($value['principal']) && $value['principal']) ? ' show active' : '' ?>" id="single-slide<?= $key ?>" role="tabpanel" aria-labelledby="single-slide-tab-<?= $key ?>">
                <div class="single-product-img easyzoom img-full">
                    <img src="<?= $value['url'] ?>" class="img-fluid" alt="">
                    <a href="<?= $value['url'] ?>" class="big-image-popup"><i class="fa fa-search-plus"></i></a>
                </div>
            </div>
        <?php endforeach; ?>
        <?php if (!empty($video)): ?>
            <div class="tab-pane fade" id="single-slide-video" role="tabpanel" aria-labelledby="single-slide-tab-video">
                <div class="single-product-img">
                    <div class="player-video" style="min-height: 388px; width: 100%;">
                        <div id="player"></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php
$js = <<<JS
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '388',
            width: '100%',
            videoId: '$video'
        });
    }

    $('#single-slide-tab-video').click(function(ev) {
        player.playVideo();
    });

    $('.small-image-slider-single-product').find('a').each(function(index, element) {
        if ($(element).attr('id') != 'single-slide-tab-video') {
            $(element).click(function(ev) {
                player.stopVideo();
            });
        }
    });
JS;
$this->registerJs($js, View::POS_END);
?>