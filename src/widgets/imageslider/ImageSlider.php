<?php

namespace kfit\theme\widgets\imageslider;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Widget para la galería de imágenes.
 *
 * @package app
 * @subpackage themes\widgets\imageslider
 * @category widgets
 *
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class ImageSlider extends \yii\base\Widget
{
    const STYLE_1 = 1;
    const STYLE_2 = 2;

    /**
     * Arreglo de las imágenes a ser mostradas y debería tener una estructura similar a:
     * ```
     * [
     *     'url' => '' // Url de la imagen
     *     'principal' => false // Define si la imagen será la principal o no
     * ]
     * ```
     * 
     * @var array
     */
    public $images;

    /**
     * Url de video para mostrar en el slider de las imágenes
     * 
     * @var string
     */
    public $video;

    /**
     * Estilo de imageSlider
     * 
     * @var int
     */
    public $style;


    /**
     * Evento de ejecución del widget
     */
    public function run()
    {
        if (empty($this->images)) {
            throw new InvalidConfigException(Yii::t('app', 'Se deben definir la propiedad "images" y su valor debe contener por lo menos una imagen.'));
        }
        if (empty($this->style)) {
            $this->style = self::STYLE_1;
        }
        $videoId = null;
        if (!empty($this->video)) {
            $start = strpos($this->video, "v=");
            $videoId = substr($this->video, $start+2, 11);
        }
        return $this->render($this->getViewName(), [
            'images' => $this->images,
            'video' => $videoId
        ]);
    }

    /**
     * Define el nombre de la vista a utilizar según el estilo.
     * 
     * @return string
     */
    public function getViewName()
    {
        $view = null;
        switch($this->style) {
            case self::STYLE_2:
                $view = 'main_2';
                break;
            default:
                $view = 'main';
                break;
        }
        return $view;
    }
}