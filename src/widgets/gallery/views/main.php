<?php
use yii\helpers\Url;
?>
<!--=============================================
	=            Featured product image gallery         =
	=============================================-->
<style>
    .featured-product-image-gallery.section-bg {
        background-position: center;
        background-size: cover;
    }

    .featured-product-image-gallery .image {
        height: 15rem;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        transition: transform .5s ease;
    }

    .featured-product-image-gallery .image:hover {
        transform: scale(1.1);
    }

    .featured-product-image-gallery .single-product-content i {
        font-size: 9rem;
    }

    @font-face {
        font-family: "Principal",
        src: url('fonts/principal-page.woff');
    }
</style>
<div class="featured-product-image-gallery mb-80 pt-120 section-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  gallery product container  =======-->
                <div class="gallery-product-container">
                    <?php foreach ($this->context->data as $key => $item) : ?>
                        <?php if (($key + 1) % 2) : ?>
                            <div class="row no-gutters">
                                <div class="col-lg-2 col-sm-0"></div>
                            <?php endif ?>

                            <div class="<?= $this->context->contentOptions['class'] ?? '' ?>">
                                <!--=======  single featured product  =======-->
                                <div class="single-featured-product">
                                    <a href="<?= Url::to($item['url']) ?>">
                                        <div class="featured-product <?= isset($item['color']) ? $item['color'] : '' ?>">
                                            <div class="single-product-content">
                                                <?php if (isset($item['image'])) { ?>
                                                    <img src="<?= $item['image'] ?>" />
                                                <?php } else if (isset($item['icon'])) { ?>
                                                    <i class="<?= $item['icon'] ?>"></i>
                                                <?php } ?>
                                            </div>
                                            <div class="single-product-footer">
                                                <?= $item['text'] ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!--=======  End of single featured product  =======-->
                            </div>

                            <?php if (($key + 1) % 2 == 0 || !isset($this->context->data[$key + 1])) : ?>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
                <!--=======  End of gallery product container  =======-->
            </div>
        </div>
    </div>
</div>
<!--=====  End of Featured product image gallery  ======-->