<?php
namespace kfit\theme\widgets\gallery;

use yii\base\Widget;

class GalleryProduct extends Widget
{
    public $bgImage;
    public $bgColor = '#255612';
    public $data = [];
    public $contentOptions = ['class' => 'col-lg-4 col-sm-6'];
    /**
     * Undocumented function
     *
     * @return void
     */
    public function run()
    {
        if (empty($this->bgImage)) {
            $this->bgImage = $this->getView()->theme->getUrl('images/banners/fullbanner-bg.jpg');
        }
        $this->getView()->registerCss('
            .featured-product-image-gallery.section-bg {
                background: ' . $this->bgColor . ',url("' . $this->bgImage . '");
            }');
        return $this->render('main');
    }
}
