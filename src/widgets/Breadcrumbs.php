<?php

namespace kfit\theme\widgets;

use app\helpers\Html;

class Breadcrumbs extends \kfit\core\widgets\Breadcrumbs
{

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();
        Html::removeCssClass($this->options, ['widget' => 'breadcrumb']);
    }
}
