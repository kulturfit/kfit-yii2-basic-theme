<?php

namespace kfit\theme\widgets\AccordianMenu;

use Yii;

/**
 * Assetbundle para el widget.
 *
 * @package app
 * @subpackage themes/finddoctor/widgets/AccordianMenu
 * @category Assetbundle
 *
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class AssetBundle extends \yii\web\AssetBundle
{
    public $sourcePath = '@theme/widgets/AccordianMenu/assets';
    public $baseUrl = '@web';
    public $css = ['main.css'];
    public $js = ['main.js'];    
}