<?php

namespace kfit\theme\widgets\AccordianMenu;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Widget para visualizar un menú tipo acordión con bootstrap4.
 *
 * @package app
 * @subpackage last package direction
 * @category widgets
 *
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class AccordianMenu extends \kfit\core\widgets\Menu
{
    /**
     * Método de ejecución del widget
     * 
     * @return mixed
     */
    public function run()
    {
        $html = '';
        AssetBundle::register($this->view);
        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');
            $html = Html::tag($tag, $this->renderItems($items));
        }
        return $html;
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item, $idSubMenu = '', $sub = false)
    {

        if (isset($item['url'])) {
            if ($item['url'] == 'javascript:;' || $item['url'] == '') {
                $template = ArrayHelper::getValue($item, 'template',
                    $this->principalLinkItem);
            } else {
                $template = ArrayHelper::getValue($item, 'template',
                    $this->linkTemplate);
            }

            if ($sub) {
                $template = '<a class="nav-link {activo}" href="{url}" {data-toggle} {data-target-submenu}><i class="material-icons">{icon}</i><span class="sidebar-mini"> {prefix} </span>
                        <span class="sidebar-normal"> {label} </span> </a>';
            }

            $class = 'collapsed';
            if ($item['active']) {
                $class = $this->activeCssClass;
            }

            return strtr($template,
                [
                    '{data-target-submenu}' => "href=\"#{$idSubMenu}\"",
                    '{data-toggle}' => !empty($idSubMenu) ? 'data-toggle="collapse"' : '',
                    '{url}' => Html::encode(Url::toRoute($item['url'])),
                    '{label}' => $item['label'],
                    '{prefix}' => $item['prefix'] ?? '',
                    '{activo}' => $class,
                    '{icon}' => isset($item['icon']) ? $item['icon'] : (isset($item['items']) ? '' : ''),
                    '{arrow}' => isset($item['items']) ? '<em class="fa fa-caret-down "></em>' : '',
                ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template',
                $this->labelTemplate);
            return strtr($template,
                [
                    '{label}' => $item['label'],
                ]);
        }
    }
}