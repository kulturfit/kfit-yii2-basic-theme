<?php
use app\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\View;
use yii\helpers\Url;
use kfit\theme\widgets\searchbar\SearchModel;

?>
<!--=============================================
	=            Search Bar          =
	=============================================-->
<div class="hero_home version_1 search-home" style="background:var(--primary-color) url(<?= Url::to('@web/images/wallpaper.jpg', true) ?>) !important">
	<div class="content">
		<h3><?= $this->context->title ?? '' ?></h3>
		<p>
			<?= $this->context->subtitle ?? '' ?>
		</p>
		<div>
			<?php
			$count = 1;
			foreach ($this->context->professions ?? [] as $profession_id => $name) {
				if ($count % 2 == 0) {
					$class = ['btn btn-warning ml-10 btn-lg'];
				} else {
					$class = ['btn btn-info ml-10 btn-lg'];
				}
				echo Html::a($name, ['/professionals/search', 'profession' => $profession_id], ['class' => $class]);
				$count++;
			}
			?>
		</div>
	</div>
</div>
<!-- /Hero -->
<!--=====  End of Search Bar   ======-->