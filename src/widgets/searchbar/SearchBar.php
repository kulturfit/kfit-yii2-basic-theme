<?php
namespace kfit\theme\widgets\searchbar;

use yii\base\Widget;

class SearchBar extends Widget
{
    public $title;
    public $subtitle;
    public $action;
    public $categoryModel;
    public $categoriesData = [];
    public $bgImage;
    public $bgColor = 'linear-gradient(rgba(76, 170, 201, 0.3), rgba(76, 170, 201, 0.3))';
    public $data = [];
    public $professions;
    /**
     * Undocumented function
     *
     * @return void
     */
    public function run()
    {
        if (empty($this->categoryModel)) {
            $this->categoryModel = new SearchModel();
        }
        if (empty($this->bgImage)) {
            $this->bgImage = $this->getView()->theme->getUrl('images/banners/fullbanner-bg.jpg');
        }
        $this->getView()->registerCss('
            .search-home {
                background: ' . $this->bgColor . ' url("' . $this->bgImage . '") !important;
            }');

        return $this->render('main');
    }
}
