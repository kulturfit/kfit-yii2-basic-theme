<?php
namespace kfit\theme\widgets\searchbar;

use yii\base\Model;
use Yii;

class SearchModel extends Model
{
    const TYPE_ALL = 'ALL';
    const TYPE_ATTORNEY = 'ATT';
    const TYPE_ACCOUNTING = 'ACC';
    public $category;
    public $type;
    public $searchText;

    public function init()
    {
        $this->type = static:: TYPE_ALL;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // category, type and searchText are required
            [['category', 'type', 'searchText'], 'required']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'category' => Yii::t('app', 'Category'),
            'type' => Yii::t('app', 'Type'),
            'searchText' => Yii::t('app', 'Search text'),
        ];
    }
}
