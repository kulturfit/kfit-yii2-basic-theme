<?php
namespace kfit\theme\widgets\slider;

use kfit\theme\assets\ThemeWidgetsAsset;
use yii\base\Widget;

class Slider extends Widget
{
    const TYPE_BRAND = 'BRAND';
    const TYPE_CATEGORY = 'CATEGORY';
    const TYPE_BEST_SELLER = 'BEST_SELLER';
    const TYPE_USER = 'USER';
    const RIBBON_TYPE_SUCCESS = 'ribbon-success';
    const RIBBON_TYPE_DANGER = 'ribbon-danger';
    public $type;
    public $title;
    public $subtitle;
    public $data;
    private $_viewFile;

    /**
     * Initializes the object.
     * This method is called at the end of the constructor.
     * The default implementation will trigger an [[EVENT_INIT]] event.
     */
    public function init()
    {
        parent::init();

        switch ($this->type) {
            case static::TYPE_BRAND:
                $this->setViewFile('brand');
                break;
            case static::TYPE_CATEGORY:
                $this->setViewFile('category');
                break;
            case static::TYPE_BEST_SELLER:
                $this->setViewFile('best_seller');
                break;
            case static::TYPE_USER:
                $this->setViewFile('user');
                break;

            default:
                $this->setViewFile('brand');
                break;
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function run()
    {
        ThemeWidgetsAsset::register($this->getView());
        return $this->render($this->viewFile, ['title' => $this->title, 'data' => $this->data]);
    }

    /**
     * Get the value of _view
     */
    public function getViewFile()
    {
        return $this->_viewFile;
    }

    /**
     * Set the value of _view
     *
     * @return  self
     */
    public function setViewFile($_viewFile)
    {
        $this->_viewFile = $_viewFile;

        return $this;
    }
}
