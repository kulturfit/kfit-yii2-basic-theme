<?php
use yii\helpers\Url;
?>
<!--=============================================
	=            Brand logo slider         =
	=============================================-->

<div class="slider brand-logo-slider mb-35">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<!--=======  blog slider section title  =======-->

				<div class="section-title">
					<h3>
						<?= $title ?></h3>
				</div>

				<!--=======  End of blog slider section title  =======-->
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<!--=======  brand logo wrapper  =======-->

				<div class="brand-logo-wrapper pt-20 pb-20">

					<?php foreach ($data as $brand) : ?>
						<!--=======  single-brand-logo  =======-->

						<div class="col">
							<div class="single-brand-logo">
									<a target="<?= (isset($brand['target'])) ? $brand['target'] : '_blank' ?>" href="<?= Url::to($brand['url']) ?>">
									<div class="image" style="background-image:url('<?= Url::to($brand['image']) ?>')"></div>
								</a>
							</div>
						</div>

						<!--=======  End of single-brand-logo  =======-->
					<?php endforeach ?>

				</div>

				<!--=======  End of brand logo wrapper  =======-->
			</div>
		</div>
	</div>
</div>

<!--=====  End of Brand logo slider  ======-->