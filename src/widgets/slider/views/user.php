<?php
use kartik\widgets\StarRating;
use yii\helpers\Url;

?>
<!--=============================================
	=            user slider         =
	=============================================-->

<div class="bg_color_1">
	<div class="container margin_120_95">
		<div class="main_title">
			<h2><?= $this->context->title ?></h2>
			<p><?= $this->context->subtitle ?></p>
		</div>
		<div id="carousel-home" class="owl-carousel owl-theme">
			<?php foreach ($this->context->data as $category) : ?>
				<div class="item">
					<a href="<?= Url::to($category['url'] ?? 'javascript:;') ?>">
						<div class="views"><span class="icon-eye-7"></span><?= $category['views'] ?? 0 ?></div>
						<div class="title">
							<h4><?= $category['name'] ?>
								<em><?= $category['type'] ?? '' ?></em>
							</h4>
						</div>
						<div style="background: url(<?= Url::to($category['image'] ?? 'javascript:;') ?>);height: 350px;background-position: center;background-repeat: no-repeat;background-size: cover;"></div>
					</a>
					<div style="display:none;">
						<?php
						// Set the language / locale for translation
						echo StarRating::widget([
							'name' => 'rating',
							'value' => $category['rating'] ?? 0,
							'language' => 'es',
							'options' => [
								'class' => 'rating-carousel'
							],
							'pluginOptions' => [
								'displayOnly' => true,
								'showCaption' => false,
								'size' => 'xs',
							],
						]);
						?>
					</div>

				</div>

			<?php endforeach ?>
		</div>
		<!-- /carousel -->
	</div>
	<!-- /container -->
</div>
<!-- /white_bg -->