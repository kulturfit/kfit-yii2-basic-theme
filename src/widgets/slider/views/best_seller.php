<?php
use yii\helpers\Url;
use yii\web\View;

/* @var $this \yii\web\View */

?>

<!--=============================================
	=            Best seller slider         =
	=============================================-->

<style>
    .slider.best-seller-slider .image {
        height: 7rem;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .best-seller-sub-product {
        cursor: pointer;
    }
</style>

<div class="slider best-seller-slider mb-35">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  category slider section title  =======-->

                <div class="section-title">
                    <h3><?= $title ?></h3>
                </div>

                <!--=======  End of category slider section title  =======-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--=======  best seller slider container  =======-->

                <div class="best-seller-slider-container pt-15 pb-15">

                    <?php foreach ($data as $key => $category) : ?>
                        <?php if (($key + 1) % 2) : ?>
                            <!--=======  single best seller product  =======-->
                            <div class="col">
                                <div class="single-best-seller-item">
                                <?php endif ?>
                                <div class="best-seller-sub-product" data-url="<?= $category['url'] ?>" onclick="reloadWindow(this)">
                                    <div class="row">
                                        <div class="col-lg-4 pl-0 pr-0">
                                            <div class="image">
                                                <a href="<?= $category['url'] ?>">
                                                    <div class="image" style="background-image:url('<?= Url::to($category['image']) ?>')"></div>
                                                    <!-- <img src="<?= $category['image'] ?>" class="img-fluid" alt=""> -->
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 pl-0 pr-0">
                                            <div class="product-content m-1">
                                                <div class="product-categories">
                                                    <?php foreach ($category['productCategories'] ?? [] as $productCategory) : ?>
                                                        <a href="<?= $productCategory['url'] ?? 'javascript:;' ?>"><?= $productCategory['name'] ?></a>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php if (isset($category['ribbon'])) : ?>
                                                    <label class="ribbon ribbon-left  <?= $category['ribbon']['type'] ?>"><?= $category['ribbon']['title'] ?></label>
                                                <?php endif; ?>

                                                <h3 class="product-title m-0 font-weight-bold"><a href="<?= $category['url'] ?>"><?= $category['name'] ?></a></h3>
                                                <p class="m-0"><?= Yii::$app->formatter->asInteger($category['quantity']) .' '. $category['unit'] ?></p>
                                                <?php if (!empty($category['price'])) : ?>
                                                    <div class="price-box">
                                                        <span class="discounted-price"><?= $category['price'] ?? '' ?></span>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if (($key + 1) % 2 == 0 || !isset($data[$key + 1])) : ?>
                                </div>
                            </div>
                        <?php endif ?>
                        <!--=======  End of single best seller product  =======-->

                    <?php endforeach ?>

                </div>

                <!--=======  End of best seller slider container  =======-->
            </div>
        </div>
    </div>
</div>
<!--=====  End of Best seller slider  ======-->
<?php
    $js = <<<JS
    function reloadWindow(elem) {
        let url = $(elem).attr('data-url');
        window.location.href = url;
    }
JS
;
$this->registerJs($js, View::POS_END);
?>