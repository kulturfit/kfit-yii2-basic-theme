	<!--=============================================
	=            category slider         =
	=============================================-->

	<div class="slider category-slider mb-35">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-12">
	                <!--=======  category slider section title  =======-->

	                <div class="section-title">
	                    <h3><?=$title?></h3>
	                </div>

	                <!--=======  End of category slider section title  =======-->

	            </div>
	        </div>
	        <div class="row">
	            <div class="col-lg-12">
	                <!--=======  category container  =======-->

	                <div class="category-slider-container">

	                    <?php foreach ($data as $category): ?>
	                    <!--=======  single category  =======-->

	                    <div class="single-category">
	                        <div class="category-image">
	                            <a href="<?=$category['url']?>" title="<?=$category['name']?>">
	                                <img src="<?=$category['image']?>" class="img-fluid" alt="">
	                            </a>
	                        </div>
	                        <div class="category-title">
	                            <h3>
	                                <a href="shop-left-sidebar.html"> <?=$category['name']?></a>
	                            </h3>
	                        </div>
	                    </div>

	                    <!--=======  End of single category  =======-->
	                    <?php endforeach?>

	                </div>

	                <!--=======  End of category container  =======-->

	            </div>
	        </div>
	    </div>
	</div>

	<!--=====  End of category slider  ======-->