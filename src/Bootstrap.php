<?php

namespace kfit\theme;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package kfit\yii2-themes
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{
    const BS4_ASSET = 'yii\bootstrap4\BootstrapAsset';
    const BSP4_ASSET = 'yii\bootstrap4\BootstrapPluginAsset';
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {

        $baseUrl = Yii::$app->assetManager->getPublishedUrl('@kfit/theme/base/');
        $app->getView()->theme->setBaseUrl($baseUrl);
        $bundles = $app->getAssetManager()->bundles;

        if (!isset($bundles[static::BS4_ASSET])) {
            $app->getAssetManager()->bundles[static::BS4_ASSET] = [
                'sourcePath' => '@kfit/theme/base',
                'css' => [
                    'css/bootstrap.min.css',
                ],
            ];
        }

        if (!isset($bundles[static::BSP4_ASSET])) {
            $app->getAssetManager()->bundles[static::BSP4_ASSET] = [
                'sourcePath' => '@kfit/theme/base',
                'js' => [
                    'js/popper.min.js',
                    'js/bootstrap.min.js',
                ],
            ];
        }
        Yii::setAlias('@theme', '@kfit/theme');
        $this->overrideViews($app);
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app)
    {
        $app->getView()->theme->pathMap['@theme/views'] = "@app/views";
    }
}
